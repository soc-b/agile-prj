const mongoose = require('mongoose')

const validator = require('validator')

const userScheme = new mongoose.SchemaType({
    name:{
        type: String,
        required: [true, 'Please provide a valid name!'],

    },
    email: {
        type:String,
        required: [true, 'Please provide your email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'Please provide a valid email'],
    },
    photo: {
        type: String,
        default: 'default.jpg',
    },
    role: {
        type: String,
        enum: ['user', 'sme', 'pharmacist', 'admin'],
        default: 'user',
    },
    password: {
        type: String,
        required: [true, 'Please provide a password'],
        minlength: 8,
        select: false,
    },
    active: {
        type: Boolean,
        default: true,
        select: false,
    },

})

const User = mongoose.model('User', userScheme)
module.exports = User